# Maintainer: psykose <alice@ayaya.dev>
pkgname=libdovi
pkgver=3.1.2
pkgrel=0
pkgdesc="Library to read & write Dolby Vision metadata"
url="https://github.com/quietvoid/dovi_tool"
arch="all"
license="MIT"
makedepends="
	cargo
	cargo-auditable
	cargo-c
	"
subpackages="$pkgname-dev"
source="https://github.com/quietvoid/dovi_tool/archive/refs/tags/libdovi-$pkgver.tar.gz"
builddir="$srcdir/dovi_tool-libdovi-$pkgver"
options="net" # cargo

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --manifest-path dolby_vision/Cargo.toml
}

build() {
	cargo auditable cbuild \
		--release \
		--frozen \
		--prefix /usr \
		--library-type cdylib \
		--manifest-path dolby_vision/Cargo.toml
}

check() {
	cargo test --frozen --manifest-path dolby_vision/Cargo.toml
}

package() {
	cargo auditable cinstall \
		--release \
		--frozen \
		--prefix /usr \
		--destdir "$pkgdir" \
		--library-type cdylib \
		--manifest-path dolby_vision/Cargo.toml
}

sha512sums="
577d5a5916dedbf222150ddb76219325e0e9a7ae91c5978b1b1fd65048d1f548e29aa8ebbbdc836380ec399e2bc105a722515f783be70837dc6403cb34586bb2  libdovi-3.1.2.tar.gz
"
