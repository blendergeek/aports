# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plank-player
pkgver=5.27.6
pkgrel=0
pkgdesc="Multimedia Player for playing local files on Plasma Bigscreen"
url="https://invent.kde.org/plasma-bigscreen/plank-player"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtgraphicaleffects
	qt5-qtmultimedia
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plank-player-$pkgver.tar.xz"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plank-player.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ff099d4167e2ac657f87d9d760ab27cfdc7b81bf732aa6572bc038cefc5d2fcdced7b7cd9d9a2915920a9bb61a9b0e6363edce35b5efb41fb12093c15004862a  plank-player-5.27.6.tar.xz
"
