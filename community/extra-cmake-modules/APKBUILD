# Contributor: k0r10n <k0r10n.dev@gmail.com>
# Contributor: Ivan Tham <pickfire@riseup.net>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=extra-cmake-modules
pkgver=5.108.0
pkgrel=1
_commit="2dee113bd3ab747245209ed1431427691dfa246d"
pkgdesc="Extra CMake modules"
url="https://invent.kde.org/frameworks/extra-cmake-modules"
arch="noarch"
license="BSD-3-Clause"
depends="cmake"
makedepends="
	py3-sphinx
	samurai
	"
checkdepends="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/extra-cmake-modules-$pkgver.tar.xz"
subpackages="$pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/extra-cmake-modules.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DSphinx_BUILD_EXECUTABLE=/usr/bin/sphinx-build \
		-DBUILD_QTHELP_DOCS=ON
	cmake --build build
}

check() {
	# KDEFetchTranslations expects KDE's git setup
	ctest --test-dir build --output-on-failure -E "KDEFetchTranslations"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a43cefed67b1266d50c8b6b9b680bea557a267b4fb9650e1a6c3dc85cb479059b2d1191179929f81ec753e49eee966754b7942b207d9214f8146335b1138f91d  extra-cmake-modules-5.108.0.tar.xz
"
