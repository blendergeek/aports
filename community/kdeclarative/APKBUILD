# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdeclarative
pkgver=5.108.0
pkgrel=1
pkgdesc="Provides integration of QML and KDE Frameworks"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kglobalaccel-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kpackage-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libepoxy-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kdeclarative.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# quickviewsharedengine requires OpenGL
	xvfb-run ctest --test-dir build --output-on-failure -E "quickviewsharedengine"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2f9f9200b18cf577cc0e67e82bd2010702c34c946533f97b0d4c2b2008b3c793ee9373058b46791003e8603e2179c6e96d42c0c57f3de66c2d2ff707331a4a5e  kdeclarative-5.108.0.tar.xz
"
