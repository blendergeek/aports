# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-cro
_pkgorig=coral-reef-optimization-algorithm
_commit=2a6d75b36b11bae91d61d78d98f28e640819fc57
pkgver=0.0.5.2
pkgrel=0
pkgdesc="Implementation of Coral Reef Optimization (CRO) Algorithm"
url="https://github.com/VictorPelaez/coral-reef-optimization-algorithm"
arch="noarch !s390x !riscv64" # py3-scikit-learn, py3-matplotlib
license="MIT"
depends="python3 py3-matplotlib py3-scipy py3-numpy py3-scikit-learn py3-pandas"
checkdepends="py3-pytest"
makedepends="py3-setuptools"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/VictorPelaez/$_pkgorig/archive/$_commit/$_pkgorig-$_commit.tar.gz"
builddir="$srcdir/$_pkgorig-$_commit"
options="!check" # missing dependency | skip for now

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
a1b6e233b321480b3d6771bdbfd1a126d66657ba3b1d54c9259e54a738564e7f3aabd727e017272d585b21c7a45d4587695917a321438f32c9240afbf0d90746  py3-cro-0.0.5.2.tar.gz
"
