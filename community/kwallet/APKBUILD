# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kwallet
pkgver=5.108.0
pkgrel=1
pkgdesc="Secure and unified container for user passwords"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	gpgme-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	kservice-dev
	kwindowsystem-dev
	libgcrypt-dev
	qca-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwallet-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
provides="dbus:org.freedesktop.Secrets"
# 2nd highst priority after gnome-keyring, mainly for KDE
provider_priority=10

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kwallet.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	local excluded_checks=
	case "$CARCH" in
		s390x) excluded_checks="(blowfishtest)";;
	esac
	ctest --test-dir build --output-on-failure -E "$excluded_checks"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ba129b8f20d1fc853075735127394629daafefd9c1c42d0a657a82ffc1634bcfa36ceddea98ac1e21fda1a4db842ae9e681cc87153571fedb3a596638d62fed7  kwallet-5.108.0.tar.xz
"
