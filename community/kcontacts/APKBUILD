# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kcontacts
pkgver=5.108.0
pkgrel=1
pkgdesc="Address book API for KDE"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
# TODO: Replace gnupg with specific gnupg subpackages that kcontacts really needs.
depends="
	gnupg
	iso-codes
	"
depends_dev="
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcontacts-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kcontacts.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kcontacts-addresstest requires Wayland display
	xvfb-run ctest --test-dir build --output-on-failure -E "kcontacts-addresstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2397363139db6d7edf466627edb5a3e6ade365a311afd177329b8ee06c0b698170c7538612184d01b5ca1656204fee77574d8248921f86e43014e08b9fd3aa8d  kcontacts-5.108.0.tar.xz
"
