# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=cargo-c
pkgver=0.9.22
pkgrel=1
pkgdesc="cargo subcommand to build and install C-ABI compatibile dynamic and static libraries"
url="https://github.com/lu-zero/cargo-c"
arch="all"
license="MIT"
# nghttp2-sys doesn't support system
makedepends="
	cargo
	cargo-auditable
	curl-dev
	libgit2-dev
	libssh2-dev
	openssl-dev>3
	zlib-dev
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/lu-zero/cargo-c/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-Cargo.lock::https://github.com/lu-zero/cargo-c/releases/download/v$pkgver/Cargo.lock"
options="net" # To download crates

export LIBSSH2_SYS_USE_PKG_CONFIG=1
export DEP_NGHTTP2_ROOT=/usr

prepare() {
	default_prepare

	cp "$srcdir"/$pkgname-$pkgver-Cargo.lock Cargo.lock

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	rm target/release/cargo-*.d
	install -Dm755 target/release/cargo-* -t "$pkgdir"/usr/bin/

	install -Dm644 -t "$pkgdir/usr/share/doc/cargo-c" README.md
}

sha512sums="
98d131300e843f730c5009c3f162e130c4c90cae87ba7a437ed7babdfcd4b68f51bfd438800c4064686b7a209facda51c659f544368a4d7a8ee6140888463288  cargo-c-0.9.22.tar.gz
2063c99327a4fe768e5ee8fd8aeb785e2f6f80fead97a124151f00376486e1a90749e8555c2747e2f1b82aa6ba6c5b3f1e05ba97734921bdd362963bf58a543d  cargo-c-0.9.22-Cargo.lock
"
