# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=oxygen-icons
pkgver=5.108.0
pkgrel=1
arch="noarch !armhf" # armhf blocked by extra-cmake-modules
pkgdesc="Oxygen icon theme"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
makedepends="$depends_dev
	extra-cmake-modules
	fdupes
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/oxygen-icons5-$pkgver.tar.xz"
builddir="$srcdir/oxygen-icons5-$pkgver"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/oxygen-icons.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0ea301ee7092682410795802d741c703d66b195a1fb7351c99255f014bc8e1fe7eb88123693d7569ef7db29b8c3e2389315eb8305343232f7ff02beb597dec2e  oxygen-icons5-5.108.0.tar.xz
"
