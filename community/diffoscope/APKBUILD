# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=245
pkgrel=0
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-libarchive-c
	py3-magic
	python3
	"
makedepends="
	py3-docutils
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	bzip2
	cdrkit
	gzip
	libarchive-tools
	openssh-client-default
	py3-html2text
	py3-pytest
	py3-pytest-xdist
	unzip
	"
subpackages="$pkgname-pyc"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# html test fails
	PYTHONDONTWRITEBYTECODE=1 \
	.testenv/bin/python3 -m pytest -n auto -k 'not test_diff'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d42d3e490636c4ac5934c6c705bc8d6951a8c4ed20b3860099ee4bf713476ee2e1829c3f5db4e22cd69d26fe2d854f9aaf87e67d98436787f822cd11a096c9ad  diffoscope-245.tar.gz
"
