# Contributor: tcely <tcely@users.noreply.github.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=botan3
pkgver=3.1.1
pkgrel=0
pkgdesc="Crypto and TLS for C++11 (v3)"
url="https://botan.randombit.net/"
arch="all"
license="BSD-2-Clause"
depends_dev="
	boost-dev
	bzip2-dev
	sqlite-dev
	xz-dev
	zlib-dev
	"
makedepends="
	$depends_dev
	python3
	"
subpackages="$pkgname-dev $pkgname-doc py3-$pkgname:py3 $pkgname-libs"
source="https://botan.randombit.net/releases/Botan-$pkgver.tar.xz"
builddir="$srcdir/Botan-$pkgver"

case "$CARCH" in
# FIXME: segfaults
x86) options="$options !check" ;;
esac

build() {
	# botan benefits from -O3. Upstream is using it when testing.
	CXXFLAGS="$CXXFLAGS -O3" \
	python3 ./configure.py \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--with-boost \
		--with-bzip2 \
		--with-lzma \
		--with-sqlite3 \
		--with-zlib \
		--with-os-feature=getrandom \
		--disable-static

	make
}

check() {
	LD_LIBRARY_PATH="$builddir" \
		timeout 600 \
		./botan-test
}

package() {
	make DESTDIR="$pkgdir" install
}

py3() {
	pkgdesc="$pkgdesc (python module)"
	depends="$pkgname-libs=$pkgver-r$pkgrel"

	amove usr/lib/python*
}

sha512sums="
ed6bdadb910b0775245648140212953ed364aa26107f851e39ac5cb664d7f476c519a22cdad41f0e520796c4ebe453c56ca68a2178e39f815d445e9979333795  Botan-3.1.1.tar.xz
"
